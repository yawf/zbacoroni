# ZBACoroni
## Typical Entities
 * **Resource** - an entity identified by a system-unique URI which identifies an abstract domain-specific resource;
 * **Action** - an entity identified by a simple string identifier which identifies a domain and resource-specific action;
 * **Security Domain** - an entity which:
   * Declares a set of (optionally wildcarded) *resources* which are governed by **this and only this** domain (resource could not be shared by different domains);
   * Therefore could grant an access to owned resources to *permission subjects* by issuing *policies*;
   * Holds a private cryptographic key to prove an identity;
   * Shares a public cryptographic key to enable anybody to verify an identity;
   * Could create *permission subjects* as resources of itself; 
 * **Permissions Subject** - an entity which:
   * Is a resource of a *security domain*;
   * Therefore a resource identifier is a permission subject's identifier;
   * Could be authenticated by a corresponding *security domain*;
   * A corresponding *security domain* could also make a *permission subject* grant its' access to other *permission subject* by issuing *policies*;
 * **Security Policy** is a decision made by a *security domain* either on its' or a *permission subject's* behalf to grant (either partially or fully) permissions to make some *actions* over a *resources*.
   Consists of:
   * An identifier;
   * *Source* - an identifier of either *security domain* or a *permissions subject*;
   * *Target* - an identifier of a *permissions subject*;
   * List of *resource's* identifiers (optionally wildcarded) along with allowed *actions*;

## Notes on permission subjects
Permissions subjects could be practically anything. Check out the examples below:
 * A group. Not intended to issue JWTs directly but to group and inherit permissions;
 * A user. Internded to issue JWTs for clients;