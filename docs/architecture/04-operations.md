# ZBACoroni
## Operations over entities
### Create a security domain
**Input** 
 * Domain identifier (or generate one and return in response);
 * Cryptographic public key;
 * Set of governed resources;
 
**Authorization**: a built-in login-password or other traditional auth;

### Create a permission subject (out of a domain)
**Input**
 * Domain identifier;
 * Permission subject's identifier;
 
**Authorization**: a JWT issued on domain's behalf and containing a cryptographic signature of a request;

### Issue a security policy
**Input**
 * Source
 * Target
 * Other policy stuff

**Authorization**: a JWT issued on source's behalf and containing a cryptographic signature of a request;

### Check an access of an entity
**Input**
 * List of resource + action pairs to check for

**Authorization**: a JWT issued on permission subject's behalf

**Description**: Here all the magic happens. We should check all the security policies' graph to validate if subject is allowed to perform the specified actions
 