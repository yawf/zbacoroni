# ZBACoroni
## Example Structure

![Example Structure Diagram](social_network_domains.svg)
Following the diagram above, the JWT issued on behalf of *User 1* should be able to access User 1 and User 2 friends while User 2 is only able to get his friends.