# ZBACoroni
## Authentication of an entities
Any authenticatable entity could be authenticated by issuing a [JWT](https://en.wikipedia.org/wiki/JSON_Web_Token) signed by a corresponding *security domain* with a specified claims:

```json
{
    "iss": "<security domain identifier>",
    "sub": "<security domain or resource subject identifier>"
}
``` 

## Authorization of requests
Some requests should be additionally authorized. We do also use JWTs to authorize such requests:
 * Adding a **unique** `id` claim to a JWT;
 * Adding a `sign` claim to a JWT which is effectively is a cryptographic signature of a request input;