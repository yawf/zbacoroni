# ZBACoroni
## A ZBAC-powered distributed security provider
### Introduction
ZBACoroni is an attempt to provide a highly flexible high-performance distributed security decision maker based on ZBAC (authori**Z**ation **B**ased **A**cess **C**ontrol) security model.

**ZBAC** stands on explicit permissions grants rather than implicit ones typical for *ABAC* and *RBAC*.


### More docs to read on
[From ABAC to ZBAC: The Evolution of Access Control Models (Alan H. Karp, Harry Haury, Michael H. Davis)](https://www.hpl.hp.com/techreports/2009/HPL-2009-30.pdf)
